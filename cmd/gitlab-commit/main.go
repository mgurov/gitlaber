package main

import (
	"encoding/base64"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/mgurov/gitlaber/internal/gitlab"
)

// TODO: doc: related to base
// TODO: doc: intended for small files
// TODO: multiple files*?
// TODO: directories?
// TODO: create if missing?

func main() {

	command := CommitChangedFiles{}
	command.setFlags()

	gitlabApi := gitlab.GitLabApi{}
	gitlabApi.SetFlags()

	flag.Parse()

	command.postParseFlags()
	if len(command.Files) == 0 {
		log.Println("No files to upload, nothing to do")
		return
	}

	if err := gitlabApi.PostParseFlags(); err != nil {
		log.Fatal(err)
	}

	err := command.commitChanged(gitlabApi)

	if err != nil {
		log.Fatal(err)
	}

}

type CommitChangedFiles struct {
	Files         []string
	GitReference  string
	CommitMessage string
	AuthorEmail   string
	AuthorName    string
	Verbose       bool
	DryRun        bool
	Force         bool
}

func (command *CommitChangedFiles) setFlags() {
	flag.StringVar(&command.GitReference, "ref", "master", "git reference to check against / push to ")
	if command.CommitMessage = os.Getenv("CI_COMMIT_MESSAGE"); command.CommitMessage == "" {
		command.CommitMessage = "check gitlaber out"
	}
	flag.StringVar(&command.AuthorEmail, "author-email", os.Getenv("GITLAB_USER_EMAIL"), "commit author email; defaults to GITLAB_USER_EMAIL env variable; committer email will be taken from the private token though")
	flag.StringVar(&command.AuthorName, "author-name", os.Getenv("GITLAB_USER_NAME"), "commit author name; defaults to GITLAB_USER_NAME env variable; committer email will be taken from the private token though")
	flag.StringVar(&command.CommitMessage, "m", command.CommitMessage, "commit message defaults to env var CI_COMMIT_MESSAGE")
	flag.BoolVar(&command.Verbose, "verbose", false, "show shit")
	flag.BoolVar(&command.DryRun, "dry-run", false, "print commit payload instead of posting it to gitlab")
	flag.BoolVar(&command.Force, "force", false, "commit unchanged files potentially creating an empty commit")
}

func (command *CommitChangedFiles) postParseFlags() {
	command.Files = flag.Args()
}

func (command CommitChangedFiles) commitChanged(gitlabApi gitlab.GitLabApi) error {
	actions := []gitlab.Action{}
	for _, f := range command.Files {

		bytes, err := ioutil.ReadFile(f)

		if err != nil {
			log.Fatalf("Could not read file %s: %v", f, err)
		}

		hasContentChanged, err := gitlabApi.HasContentChanged(f, "master", bytes, command.Verbose)
		if err != nil {
			log.Fatalf("Couldn't determine content status of %s: %v", f, err)
		}

		if !hasContentChanged && !command.Force {
			if command.Verbose {
				log.Printf("%s hasn't changed, skipping", f)
			}
			continue
		}

		actions = append(actions, gitlab.Action{
			Action:   "update",
			FilePath: f,
			Encoging: "base64",
			Content:  base64.StdEncoding.EncodeToString(bytes),
		})
	}

	if len(actions) == 0 {
		log.Println("No files have changed, nothing to do")
		return nil
	}

	commit := gitlab.Commit{
		Branch:      command.GitReference,
		Message:     command.CommitMessage,
		AuthorEmail: command.AuthorEmail,
		AuthorName:  command.AuthorName,
		Actions:     actions,
	}

	responseBody, err := gitlabApi.PostCommit(commit, command.Verbose, command.DryRun)
	if nil != err {
		log.Fatalf("posting commit: %v", err)
	}

	fmt.Println(string(responseBody))
	return nil
}
