package gitlab

import (
	"crypto/sha256"
	"fmt"
	"log"
	"net/http"
	"net/url"

	"gitlab.com/mgurov/gitlaber/internal"
)

//HasContentChanged uses repositories files API (https://docs.gitlab.com/ee/api/repository_files.html)
// to examine whether a gitlab resource contained at path is different than the bytes provided by the means of sha256 comparison
func (g GitLabApi) HasContentChanged(
	path string,
	ref string,
	bytes []byte,
	verbose bool,
) (bool, error) {
	url := g.RepositoryUrl() + "/files/" + url.PathEscape(path) + "?ref=" + url.QueryEscape(ref)

	if verbose {
		//TODO: http util dump?
		log.Println("HEAD", url)
	}

	httpRequest, err := http.NewRequest("HEAD", url, nil)
	if err != nil {
		return false, fmt.Errorf("creating http request: %v", err)
	}
	g.ApplyToken(httpRequest)
	httpRequest.Header.Add("Content-type", "application/json")

	_, httpResponse, err := internal.DoHttpRequest(httpRequest)
	if err != nil {
		return false, fmt.Errorf("fetching HEAD status: %v", err)
	}
	responseSha256 := httpResponse.Header.Get("X-Gitlab-Content-Sha256")
	if "" == responseSha256 {
		return false, fmt.Errorf("missing X-Gitlab-Content-Sha256 response header")
	}

	fileSha265 := fmt.Sprintf("%x", sha256.Sum256(bytes))
	return fileSha265 != responseSha256, nil
}
