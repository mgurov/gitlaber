package gitlab

import (
	"errors"
	"flag"
	"fmt"
	"net/http"
	"os"
)

type GitLabApi struct {
	PrivateToken string
	ApiBase      string
	ProjectId    string
}

func (g GitLabApi) RepositoryUrl() string {
	return fmt.Sprintf("%s/projects/%s/repository", g.ApiBase, g.ProjectId)
}

func (g GitLabApi) ApplyToken(httpRequest *http.Request) {
	httpRequest.Header.Add("PRIVATE-TOKEN", g.PrivateToken)
}

func (g *GitLabApi) SetFlags() {
	g.ApiBase = os.Getenv(CI_API_V4_URL)
	if g.ApiBase == "" {
		g.ApiBase = "https://gitlab.com/api/v4"
	}
	flag.StringVar(&g.ApiBase, "gitlab-api", g.ApiBase, "gitab API URL; fallbacks to "+CI_API_V4_URL+" env var")

	g.ProjectId = os.Getenv(CI_PROJECT_ID)
	flag.StringVar(&g.ProjectId, "gitlab-project", os.Getenv(CI_PROJECT_ID), "gitab Project ID; defaults to "+CI_PROJECT_ID+" env var")

	flag.StringVar(&g.PrivateToken, "private-token", "", "gitab Project ID; defaults to "+GITLAB_CI_ACCESS_TOKEN+" env var")
}

func (g *GitLabApi) PostParseFlags() error {
	if "" == g.ApiBase {
		return errors.New("Missing GitLab API URL")
	}
	if "" == g.ProjectId {
		return errors.New("Missing GitLab Project ID")
	}

	if "" == g.PrivateToken {
		g.PrivateToken = os.Getenv("GITLAB_CI_ACCESS_TOKEN")
	}
	if "" == g.PrivateToken {
		return errors.New("Missing GitLab Private Token")
	}

	return nil
}

const GITLAB_CI_ACCESS_TOKEN = "GITLAB_CI_ACCESS_TOKEN"
const CI_API_V4_URL = "CI_API_V4_URL"
const CI_PROJECT_ID = "CI_PROJECT_ID"
