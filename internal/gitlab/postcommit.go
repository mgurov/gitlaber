package gitlab

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/mgurov/gitlaber/internal"
)

func (g GitLabApi) PostCommit(commit Commit, verbose, dryRun bool) ([]byte, error) {
	commitBytes, err := json.Marshal(commit)
	if err != nil {
		return nil, fmt.Errorf("marshalling commit: %v", err)
	}

	url := g.RepositoryUrl() + "/commits"

	if verbose {
		//TODO: http dump?
		log.Println("POST", url)
		log.Println(string(commitBytes))
	}

	httpRequest, err := http.NewRequest("POST", url, bytes.NewBuffer(commitBytes))
	if err != nil {
		return nil, fmt.Errorf("creating http request: %v", err)
	}
	g.ApplyToken(httpRequest)
	httpRequest.Header.Add("Content-type", "application/json")

	if dryRun {
		return commitBytes, nil
	}

	responseBody, _, err := internal.DoHttpRequest(httpRequest)
	if err != nil {
		return nil, fmt.Errorf("posting commit: %v", err)
	}

	return responseBody, nil
}

type Action struct {
	Action   string `json:"action"`
	FilePath string `json:"file_path"`
	Encoging string `json:"encoding"`
	Content  string `json:"content"`
}

type Commit struct {
	Branch      string   `json:"branch"`
	Message     string   `json:"commit_message"`
	AuthorEmail string   `json:"author_email"`
	AuthorName  string   `json:"author_name"`
	Actions     []Action `json:"actions"`
}
