package internal

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

//DoHttpRequest does http request, reads response body and returns error if response status code different for 2xx
func DoHttpRequest(httpRequest *http.Request) ([]byte, *http.Response, error) {
	httpResponse, err := http.DefaultClient.Do(httpRequest)
	if err != nil {
		return nil, nil, fmt.Errorf("sending http request: %v", err)
	}

	responseBody, err := ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		return nil, nil, fmt.Errorf("reading response code (status %d): %v", httpResponse.StatusCode, err)
	}

	if httpResponse.StatusCode/100 != 2 {
		//TODO: specific error here
		return responseBody, httpResponse, fmt.Errorf("non-2xx http response code %d: %s", httpResponse.StatusCode, string(responseBody))
	}

	return responseBody, httpResponse, nil
}
