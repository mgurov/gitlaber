# gitlaber

simple convenince helper to gitlab API

# gitlab-commit 

`gitlab-commit` `POST`s a new commit based on the provided list of files, e.g.: 

```
gitlab-commit config1.yml config2.yml
```

will create a new commit in current repository's `master` with changed `config1.yml` or `config2.yml` files. No commit will be performed if there are no changes against the target repository. 

Features: 
* API only, no git checkout needed
* POST's only changed files by default, omits the commit on no modifications

Gitlab [Commits API](https://docs.gitlab.com/ee/api/commits.html#create-a-commit-with-multiple-files-and-actions) is used to create the commit. 

Gitlab [Repository files API](https://docs.gitlab.com/ee/api/repository_files.html#get-file-from-repository) is used to fetch sha256 checksums of the target file(s).

## Parameters

### List of files 

Should be at the end of the optional parameters. 

Only update of the existing files in the target repository is supported at the moment.

Should be relative to the current directory and the same path would be used in the target repository, e.g. `dir/file.ex` will post `file.ex` from the `dir` directory to the `dir/file.ex` path at the target repository.

Files should be specified explicitly. Recursion into the directories isn't supported. Shell will usually expand `dir/*.yml` into all the `.yml` files under `dir` though.

NB: the tool isn't optimized for big data volumes.

### Private Token 

Use `--private-token=BLAHBLAHBLAH` to specify your secret. Otherwise picked from the custom `GITLAB_CI_ACCESS_TOKEN` env variable. 

NB: you'll want to keep this hidden from unsolicited eyes.

### Project ID 

`--gitlab-project=NNN` specifies the target repository. Defaults to `CI_PROJECT_ID` which would be the current repository within GitLab CI.

### Git Reference

`--ref=my-branch` with default `master`

### GitLab API URL 

`--gitlab-api=https://gitlab.host/api/v4` defaults to the value of `CI_API_V4_URL` normally available within a GitLab CI job, or `https://gitlab.com/api/v4` as the fallback.

### Git Commit Message

`-m "my commit message"` will be picked from the standard GitLab CI `CI_COMMIT_MESSAGE` env variable or will advertise gitlaber by default.

### Dry Run 

`--dry-run` or `--dry-run=true` will print the json payload to the standard output instead of POSTing it to the GitLab Commits API. 

### Force commit

`--force` will create a commit even if no files have changed.

### Verbosity 

Use `--verbose` to see more

### Author Email and Username 

`author-email=blah@fooe` defaults to `GITLAB_USER_EMAIL` env variable whereas `author-name` defaults to `GITLAB_USER_NAME`

## Precautions

* The tool isn't optimized for big data volumes

## Future developments (potential)

* `POST` content from stdin.
* `POST` json from stdin.